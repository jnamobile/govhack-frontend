﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameChecker {

	public Camera camera;
	
	void Start() {
		if (camera == null) camera = Camera.main;
	}
	public static bool IsInFrame(GameObject o, Camera cam) {
		MeshRenderer r = o.gameObject.GetComponentInChildren<MeshRenderer>();
		if (r != null) return IsRendererInFrame(r,  cam);
		Collider c = o.gameObject.GetComponentInChildren<Collider>();
		if (c != null) return IsColliderInFrame(c,  cam);
		return false;
	}

	public static bool IsColliderInFrame(Collider c, Camera cam) {
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
		if (GeometryUtility.TestPlanesAABB(planes , c.bounds))
			return true;
		else
			return false;
	}

	public static bool IsRendererInFrame(MeshRenderer r, Camera cam) {
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);
		if (GeometryUtility.TestPlanesAABB(planes , r.bounds))
			return true;
		else
			return false;
	}
}
