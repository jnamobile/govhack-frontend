﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebHelper : MonoBehaviour {
	
	public delegate void WWWCallBack(WWW www);

	/// <summary>
	/// Do a WWW GET request.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="getVariables">Get variables (parameters).</param>
	/// <param name="successCallback">Success callback.</param>
	/// <param name="failureCallback">Optional Failure callback.</param>
	/// <param name="sessionCookie">Optional Session cookie.</param>
	public WWW Get(string url, Dictionary<string, string> getVariables, WWWCallBack successCallback, WWWCallBack failureCallback = null, string sessionCookie = null)
	{
		string completeUrl = url;
		if (getVariables != null && getVariables.Count > 0) completeUrl += "?";
		foreach(KeyValuePair<string,string> getVariable in getVariables)
		{
			completeUrl += getVariable.Key + "=" + getVariable.Value + "&";
		}

		Dictionary <string, string> requestHeaders = new Dictionary<string, string>();
		if (sessionCookie != null) requestHeaders.Add("Cookie", sessionCookie);

		WWW www = new WWW (completeUrl, null, requestHeaders);

		StartCoroutine (WaitForRequest (www, successCallback, failureCallback));
		return www; 
	}
	/// <summary>
	/// Do a WWW POST request.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="getVariables">Get variables (parameters).</param>
	/// <param name="successCallback">Success callback.</param>
	/// <param name="failureCallback">Optional Failure callback.</param>
	/// <param name="sessionCookie">Optional Session cookie.</param>
	public WWW Post(string url, Dictionary<string,string> post, WWWCallBack successCallback, WWWCallBack failureCallback = null, string sessionCookie = null)
	{
		WWWForm form = new WWWForm();
		foreach(KeyValuePair<string,string> post_arg in post)
		{
			form.AddField(post_arg.Key, post_arg.Value);
		}

		Dictionary <string, string> requestHeaders = new Dictionary<string, string>();
		if (sessionCookie != null) requestHeaders.Add("Cookie", sessionCookie);

		WWW www = new WWW(url, form.data, requestHeaders);

		StartCoroutine(WaitForRequest(www, successCallback, failureCallback));
		return www; 
	}

	/// <summary>
	/// Waits for request and calls success of failure callback
	/// </summary>
	/// <returns>The for request.</returns>
	/// <param name="www">Www.</param>
	/// <param name="successCallback">Success callback.</param>
	/// <param name="failureCallback">Failure callback.</param>
	private IEnumerator WaitForRequest(WWW www, WWWCallBack successCallback, WWWCallBack failureCallback)
	{
		yield return www;
		if (www.error == null) 
		{
			successCallback(www);
		}  
		else
		{
			if (failureCallback != null) failureCallback(www);
		}  
	}


	/// <summary>
	/// Md5 equivalent to php md5() function.
	/// From Unity wiki.
	/// </summary>
	/// <param name="stringToHash">String to hash.</param>
	public static string Md5(string stringToHash)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(stringToHash);

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";
		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}

		return hashString.PadLeft(32, '0');
	}
}
