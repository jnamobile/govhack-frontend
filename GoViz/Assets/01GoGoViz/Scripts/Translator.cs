﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;

public class Translator : MonoBehaviour {

	public Animation translating;

	public string serverUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate";

	public string apiKey = "trnsl.1.1.20180908T053943Z.1a62f2a8043be865.1bfadcc6e6585e3fc7a1f8bea8ae3b7fc8922329";

	public string toLang = "fr";

	// Update is called once per frame
	public void TranslateText (Text textField) {
		string original = textField.text;
		WebHelper helper = GetComponent<WebHelper>();
		Dictionary<string,string> vars = new Dictionary<string, string>();
		vars.Add("key", apiKey);
		vars.Add("text", original);
		vars.Add("lang", "en-" + toLang);
		if (translating != null) translating.Play();
		helper.Get(serverUrl, vars, (WWW www) => {
			if (www == null || www.text == null || www.text == "") {
				Debug.LogError("WWW result was blank"); 
				return;
			}
			try {
				Debug.Log("Results: " + www.text);
				JsonData d = JsonMapper.ToObject(www.text);
				textField.text = d["text"][0].ToString();
			} catch (System.Exception) {
				Debug.Log ("Failed to parse JSON Data");
			}
			if (translating != null) translating.Stop();
		}, (WWW www) => { 
			Debug.LogError("Error translating: " + www.error); 
			if (translating != null) translating.Stop();
		});
	}

	

}
