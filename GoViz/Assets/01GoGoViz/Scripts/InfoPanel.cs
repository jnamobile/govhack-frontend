﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InfoPanel : GoGoPanel {

	public Image image;
	public Text headingText;
	public Text contentText;
	public Sprite defaultSprite;

	public Image mapMarker;
	public Button mapButton;

	public Translator translator;

	public void UpdateAndShow(string name) {
		UpdateContents(ARMarkerData.Instance.GetMarkerForId(name));
		Show();
	}

	public void UpdateContents(MarkerData data) {
		if (data == null) return;
		// Sprite sprite, string heading, string content) {
		if (data.image != null) {
			image.sprite = data.image;
		} else {
			image.sprite = defaultSprite;
		}
		headingText.text = data.header;
		contentText.text = data.body;
		Debug.Log("Factory: " + data.factory);
		if (data.factory != null) {
			mapButton.interactable = true;
			mapMarker.enabled = true;
			data.factory.SpawnNow();
			
		} else {
			// mapButton.interactable = false;
			mapMarker.enabled = false;
		}
	}

	public void Translate() {
		translator.TranslateText(headingText);
		translator.TranslateText(contentText);
	}
}
