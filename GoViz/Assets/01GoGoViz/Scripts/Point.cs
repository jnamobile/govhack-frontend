using System.Collections.Generic;

[System.Serializable]
public class PointData {

    public int id;
    public string name;
    public double longitude;
    public double latitude;
    public List<double> xData;
    public List<double> yData;
    public List<string> stringData;

    public List<int> commentIds;
}
