﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.Examples.AugmentedImage;

public class InfoPanelVis : GoGoPanel {

	public Image image;
	public Text headingText;
	public Text userText;
	public Text additionalText;
	AugmentedImageVisualizer visualiser;

	public void UpdateContents(MarkerData data, AugmentedImageVisualizer visualiser) {
		this.visualiser = visualiser;
		if (data == null) return;
		// Sprite sprite, string heading, string content) {	
		headingText.text = data.header;
		additionalText.text = data.body;
		userText.text = data.user;
	}

}
