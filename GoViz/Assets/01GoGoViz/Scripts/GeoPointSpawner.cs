﻿using Mapbox.Examples;
using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Factories;
using Mapbox.Unity.Utilities;
using System.Collections.Generic;
using System.Collections;

public class GeoPointSpawner : MonoBehaviour
{
	[SerializeField]
	AbstractMap _map;

	Vector2d[] _locations;

	[SerializeField]
	float _spawnScale = 100f;

	[SerializeField]
	GameObject _markerPrefab;

	[SerializeField]
	GameObject _closestMarkerPrefab;

	[SerializeField]
	DataFactory dataFactory;

	List<GameObject> _spawnedObjects;

	public bool updateMapLocationPostSpawn;

	public void Spawn() {
		Debug.Log("Starting spawn");
		StartCoroutine(DoSpawn());
	}

	IEnumerator DoSpawn()
	{
		while(!dataFactory.IsReady) yield return true;
		
		_locations = new Vector2d[dataFactory.points.Count];
		_spawnedObjects = new List<GameObject>();
		for (int i = 0; i < dataFactory.points.Count; i++)
		{
			_locations[i] = new Vector2d(dataFactory.points[i].latitude, dataFactory.points[i].longitude);
			var instance = Instantiate(_markerPrefab);
			instance.transform.localPosition = _map.GeoToWorldPosition(_locations[i], true);
			instance.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
			_spawnedObjects.Add(instance);
		}
#if !UNITY_EDITOR
		if (updateMapLocationPostSpawn) WifiARAndMapController.Instance.UpdateLocation();
#endif
	}

	private void Update()
	{
		if (_spawnedObjects != null && _spawnedObjects.Count > 0)
		{
			int count = _spawnedObjects.Count;
			for (int i = 0; i < count; i++)
			{
				var spawnedObject = _spawnedObjects[i];
				var location = _locations[i];
				spawnedObject.transform.localPosition = _map.GeoToWorldPosition(location, true);
				spawnedObject.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
			}
		}
	}
}