﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARMarkerData : MonoBehaviour {

	public static ARMarkerData Instance {
		get; protected set;
	}

	public List<MarkerData> markerData;
	
	void Awake() {
		Instance = this;
	}
	public MarkerData GetMarkerForId(string imageId) {
		foreach (MarkerData m in markerData) {
			Debug.Log(imageId + " -> " + m.name);
			if (m != null && string.Equals(m.name, imageId, System.StringComparison.OrdinalIgnoreCase)) return m;
		}
		return null;
	}
}

[System.Serializable]
public class MarkerData {
	public string name;
	public string header;
	[TextArea]
	public string body;
	public string user;
	public Sprite image;
	public DataFactory factory;
	public InfoPanelVis altVisualisation;
}