﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateDriver : MonoBehaviour {

	public GameObject[] steps;

	int currentStep = 0;

	public void NextStep() {
		steps[currentStep].SetActive(false);
		currentStep++;
		if (currentStep >= steps.Length) currentStep = 0;
		steps[currentStep].SetActive(true);
	}
}
