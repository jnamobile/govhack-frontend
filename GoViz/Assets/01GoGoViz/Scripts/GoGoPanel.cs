using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoGoPanel : MonoBehaviour {

    public static GoGoPanel CurrentPanel;

    public GameObject[] visibleContents;

    public Button myButton;

    public AppState myState;

    public void Show() {
        if (CurrentPanel != null) CurrentPanel.Hide();
        foreach (GameObject v in visibleContents) {
            v.SetActive(true);
        }
        CurrentPanel = this;
        if (myButton != null) myButton.enabled = false;
        WifiARAndMapController.Instance.State = myState;
    }

    public void Hide() {
        foreach (GameObject v in visibleContents) {
            v.SetActive(false);
        }
        if (myButton != null) myButton.enabled = true;
    }

}
