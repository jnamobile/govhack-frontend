﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using Mapbox.Unity.Location;
using Mapbox.Utils;

public class DataFactory : MonoBehaviour {

	public string serverUrl = "http://141.132.124.17:8080/";

	public string dataset = "FREE_WIFI";

	public int distance = 10000;

	public List<PointData> points;

	public bool testMode;		// In test mode we don't get data from the server

	public bool IsReady {
		get; protected set;
	}

	public GeoPointSpawner spawner;

	public bool isGeoFeature;

	WebHelper helper;

	// Use this for initialization
	void Start () {
		if (testMode) {
			IsReady = true;
		} else {
			StartCoroutine(LoadData());
		}
	}

	IEnumerator LoadData() {
		
		// Wait for location provider
		yield return true;
		yield return true;
#if UNITY_EDITOR 
		Vector2d longLat = new Vector2d();
		longLat.y = 143.79;
		longLat.x = -37.5;

#else
		yield return new WaitForSeconds(5);
		ILocationProvider locationProvider = LocationProviderFactory.Instance.DeviceLocationProvider;
		Vector2d longLat = locationProvider.CurrentLocation.LatitudeLongitude;
		// longLat.x = 143.79;
		// longLat.y = -37.5;
#endif
		Debug.Log(longLat.x + " " + longLat.y);

		helper = GetComponent<WebHelper>();
		string url = serverUrl + "query/" + dataset +"/nearby";
		if (isGeoFeature) url = serverUrl;
		Dictionary<string,string> vars = new Dictionary<string, string>();

		if (isGeoFeature) {
			// Use geo backend
			helper.Get(url, vars,  GeoDataSetLoaded, (WWW www) => { 
				Debug.LogError("Error getting data set: " + www.error); 
			});
		} else {
			vars.Add("distance","" + distance);
			vars.Add("latitude","" + longLat.x);
			vars.Add("longitude","" + longLat.y);
			// Use our backend
			helper.Get(url, vars,  DataSetLoaded, (WWW www) => { 
				Debug.LogError("Error getting data set: " + www.error); 
			});
		}
		
	}
	
	void DataSetLoaded(WWW www) {
		if (www == null || www.text == null || www.text == "") {
			Debug.LogError("WWW result was blank"); 
			return;
		}
		try {
			Debug.Log("Results: " + www.text);
			points = JsonMapper.ToObject<List<PointData>>(www.text);
			IsReady = true;
		} catch (System.Exception) {
			Debug.Log ("Failed to parse JSON Data");
		}
	}

	void GeoDataSetLoaded(WWW www) {
		if (www == null || www.text == null || www.text == "") {
			Debug.LogError("WWW result was blank"); 
			return;
		}
		try {
			JsonData data = JsonMapper.ToObject(www.text);
			foreach (JsonData feature in data["features"]) {
				PointData p = new PointData();
				p.name = feature["properties"]["Site Name"].ToString();
				System.Double.TryParse(feature["geometry"]["coordinates"][0].ToString(), out p.longitude);
				System.Double.TryParse(feature["geometry"]["coordinates"][1].ToString(), out p.latitude );
				p.stringData = new List<string>();
				p.stringData.Add(feature["properties"]["Feature Location"].ToString());
				points.Add(p);
			}
			IsReady = true;
		} catch (System.Exception) {
			Debug.Log ("Failed to parse JSON Data");
		}
	}


	public void SpawnNow() {
		Debug.Log("Starting spawn: " + spawner);
		if (spawner != null) spawner.Spawn();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
