//-----------------------------------------------------------------------
// ORIGNAL SOURCE: <copyright file="AugmentedImageExampleController.cs" company="Google">
//
// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Runtime.InteropServices;
using GoogleARCore;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using GoogleARCore.Examples.AugmentedImage;


/// <summary>
/// Controller for AR Marker example
/// </summary>
public class WifiARAndMapController : MonoBehaviour
{
    ILocationProvider locationProvider;
    
    public static WifiARAndMapController Instance
    {
        get; protected set;
    }
    
    private AppState state;
    public AppState State {
        get {
            return state;
        }
        set {
            state = value;
            switch (state) {
                case AppState.SCANNING:
                break;
                case AppState.INFO:
                break;
                case AppState.MAP:
                break;
                case AppState.COMMENTS:
                break;
                case AppState.INIT:
                break;
            }
        }
    }

    public GoGoPanel scanPanel;
    public GoGoPanel mapPanel;

    public InfoPanel infoPanel;


    public GoGoPanel commentPanel;

    public AbstractMap map;

    /// <summary>
    /// A prefab for visualizing an AugmentedImage.
    /// </summary>
    public AugmentedImageVisualizer AugmentedImageVisualizerPrefab;

    /// <summary>
    /// The overlay containing the fit to scan user guide.
    /// </summary>
    public GameObject FitToScanOverlay;

    public bool useVisualiser;

    private Dictionary<int, AugmentedImageVisualizer> m_Visualizers
        = new Dictionary<int, AugmentedImageVisualizer>();

    private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

    void Awake() {
        Instance = this;
        state = AppState.SCANNING;
        GoGoPanel.CurrentPanel = scanPanel;
    }

    /// <summary>
    /// The Unity Update method.
    /// </summary>
    public void Update()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        DoTracking();
    }
 
    void DoTracking() {
        // Check that motion tracking is tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }
        // UpdateLocation();

        // Get updated augmented images for this frame.
        Session.GetTrackables<AugmentedImage>(m_TempAugmentedImages, TrackableQueryFilter.Updated);

        // Create visualizers and anchors for updated augmented images that are tracking and do not previously
        // have a visualizer. Remove visualizers for stopped images.
        foreach (var image in m_TempAugmentedImages)
        {
            AugmentedImageVisualizer visualizer = null;
            m_Visualizers.TryGetValue(image.DatabaseIndex, out visualizer);
            if (image.TrackingState == TrackingState.Tracking && visualizer == null)
            {
                
                if (image.Name != null && image.Name != "") {
                    string name = "" + image.Name;
                    MarkerData data = ARMarkerData.Instance.GetMarkerForId(name);
                    
                    
                    if (data != null) {
                        // Create an anchor to ensure that ARCore keeps tracking this augmented image.
                    Anchor  anchor = image.CreateAnchor(image.CenterPose);
                        if (data.altVisualisation) {
                            visualizer = (AugmentedImageVisualizer)Instantiate(AugmentedImageVisualizerPrefab, anchor.transform);
                            visualizer.Image = image;
                            m_Visualizers.Add(image.DatabaseIndex, visualizer);
                            visualizer.gameObject.SetActive(true);
                            Canvas c = data.altVisualisation.GetComponentInParent<Canvas>();
                            c.transform.parent = visualizer.transform;
                            c.transform.localPosition = Vector3.zero;
                            c.transform.localEulerAngles = new Vector3(90.0f,0,0);
                            data.altVisualisation.UpdateContents(data, visualizer);
                            data.altVisualisation.Show();
                        } else {
                            visualizer = (AugmentedImageVisualizer)Instantiate(AugmentedImageVisualizerPrefab, anchor.transform);
                            visualizer.Image = image;
                            m_Visualizers.Add(image.DatabaseIndex, visualizer);
                            visualizer.gameObject.SetActive(true);
                            infoPanel.UpdateContents(data);
                            infoPanel.Show();
                        }
                    } else {
                        Debug.LogError("Marker data was null: " + name);
                    }
                    
                }
            }
            else if (image.TrackingState == TrackingState.Stopped && visualizer != null)
            {
                m_Visualizers.Remove(image.DatabaseIndex);
                GameObject.Destroy(visualizer.gameObject);
            }
            else if (visualizer != null && visualizer.removeWhenOutOfFrame)
            {
                if (!FrameChecker.IsInFrame(visualizer.FrameLowerRight, Camera.main) ||
                    !FrameChecker.IsInFrame(visualizer.FrameLowerLeft, Camera.main) ||
                    !FrameChecker.IsInFrame(visualizer.FrameUpperRight, Camera.main) ||
                    !FrameChecker.IsInFrame(visualizer.FrameUpperLeft, Camera.main)) {
                    visualizer.gameObject.SetActive(false);
                    } else {
                        visualizer.gameObject.SetActive(true);
                    }
            }
        }

        // Show the fit-to-scan overlay if there are no images that are Tracking.
        foreach (var visualizer in m_Visualizers.Values)
        {
            if (visualizer.Image.TrackingState == TrackingState.Tracking)
            {
                FitToScanOverlay.SetActive(false);
                return;
            }
        }

        if (state == AppState.SCANNING) FitToScanOverlay.SetActive(true);
    }


    public void UpdateLocation() {
        if (locationProvider == null) locationProvider = LocationProviderFactory.Instance.DeviceLocationProvider;
        if (map == null) map = FindObjectOfType<AbstractMap>();
        if (locationProvider != null && map != null) {
            map.UpdateMap(locationProvider.CurrentLocation.LatitudeLongitude, map.Zoom);
        }
    }

   
}


 public enum AppState {
        INIT,
        SCANNING,
        INFO,
        MAP, 
        COMMENTS
    }